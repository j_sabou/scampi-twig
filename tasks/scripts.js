//
// Scripts
// ----------------------------------------------------------------------------

'use strict';

const gulp         = require('gulp');
const config       = require('../config.json');
const concat       = require('gulp-concat');


// copy:js-vendors
// Envoie les dépendences js dans public
// ----------------------------------------------------------------------------
// gulp.task('copy:js-vendors', () => {
//   return gulp.src(config.paths.assets + 'scampi/scripts/has-js.js')
//     .pipe(gulp.dest(config.paths.build + 'assets/scripts/vendors'));
// });

// make:js
// Concaténation des scripts du projet (scripts/main/)
// ----------------------------------------------------------------------------

gulp.task('make:js-main', () => {
  return gulp.src([
    // ajout des dépendences à scampi
    './node_modules/@pidila/scampi/modules/svg-icons/svgxuse.js',
    './node_modules/@pidila/scampi/modules/collapse/index.js',
    './node_modules/@pidila/scampi/modules/skip-link/index.js',
    './node_modules/@pidila/scampi/modules/u-palette/index.js',
    // scripts spécifiques au projet
    config.paths.assets + 'project/scripts/main/*.js'
  ])
    .pipe(concat('main.js'))
    .pipe(gulp.dest(config.paths.build + 'assets/scripts'))
});
