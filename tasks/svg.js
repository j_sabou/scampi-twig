//
// SVG
// ----------------------------------------------------------------------------

'use strict';

const gulp         = require('gulp');
const config       = require('../config.json');
const svgSprite    = require('gulp-svg-sprite');
const plumber      = require('gulp-plumber');

// var gulp         = require('gulp-help')(require('gulp'));
// var config       = require('../config.json');
// var svgSprite    = require('gulp-svg-sprite');
// var plumber      = require('gulp-plumber');


// config
// ----------------------------------------------------------------------------

var svgSpriteconfig = {
  shape                 : {
    dimension           : { // Set maximum dimensions
      maxWidth          : 64,
      maxHeight         : 64
    },
    dest                : 'unitaires' // Keep the intermediate files
  },
  mode                  : {
    symbol              : { // Activate the «symbol» mode
      render            : {
        css             : false, // CSS output option for icon sizing
        scss            : false // SCSS output option for icon sizing
      },
      dest              : ".",
      sprite            : "icon-sprite.svg", // Sprite path and name
      example           : true // Build a sample page, please!
    }
  },
  svg                   : {
    doctypeDeclaration  : false,
    dimensionAttributes : false
  }
};


// prep:sprite
// Fabrication du sprite svg
// ----------------------------------------------------------------------------

gulp.task('prep:sprite', () => {
  return gulp.src(config.paths.assets + 'project/icones/unitaires/*')
    .pipe(plumber())
    .pipe(svgSprite(svgSpriteconfig))
    .pipe(gulp.dest(config.paths.assets + 'project/icones/'));
});
// Si les icones n'ont pas déjà été passées toutes en noir à la prépa,
// nettoyer le sprite avec chercher remplacer Sublime expression régulière
// fill="#[0-9A-F]{6}"
