//
// SVG
// ----------------------------------------------------------------------------

'use strict';

const gulp        = require('gulp');
const config      = require('../config.json');
const browserSync = require("browser-sync").create();


// live
// Lance l'index dans le navigateur et surveille les modifs
// ----------------------------------------------------------------------------

gulp.task('live', (done) => {

  browserSync.init({
    server: config.paths.build
  });

  gulp.watch(
    [config.paths.pages + '**/*.twig',
      config.paths.templates + '**/*.twig',
      './*.md'],
    gulp.series('make:html')
  );

  gulp.watch(
    [config.paths.assets + 'project/scss/**/*.scss'],
    gulp.series('make:css')
  );

  gulp.watch(config.paths.build + '**/*.html');
  gulp.watch(config.paths.build + '**/*.css').on('change', browserSync.reload);

  done();

});
