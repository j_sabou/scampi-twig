//
// Clean
// ----------------------------------------------------------------------------


const gulp = require('gulp');
const del  = require('del');

// clean
// Supprime le répertoire `public`
// ----------------------------------------------------------------------------

gulp.task('clean',  () => {
  return del(['public']);
});
