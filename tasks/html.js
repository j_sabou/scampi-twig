//
// html
// ----------------------------------------------------------------------------

'use strict';

const gulp          = require('gulp');
const config        = require('../config.json');
const configPackage = require('../package.json');
const configScampi  = require('@pidila/scampi/package.json');
const argv          = require('yargs').argv;

const twig          = require('gulp-twig');
const twigMarkdown  = require('twig-markdown');
const prettify      = require('gulp-jsbeautifier');


// variables selon l'environnement
if (argv.prod) {
  var env = "prod",
      baseURL = config.baseURL.prod;
}
else {
  var baseURL = config.baseURL.dev;
}

// make:html
// Compile twig vers html
// ----------------------------------------------------------------------------

gulp.task('make:html', () => {
  return gulp.src(config.paths.pages + '**/*.twig')
    .pipe(twig({
      base: config.paths.templates,
      data: {baseURL, env, config, configPackage, configScampi},
      extend: twigMarkdown
    }))
    .pipe(gulp.dest(config.paths.build));
  },
  {
    options: {
      'dev': '[par défaut] baseURL = config.baseURL.dev',
      'prod': 'baseURL = config.baseURL.dev',
    }
  }
);


// make:html-prettify
// Normalise l'indentation du html
// ----------------------------------------------------------------------------

gulp.task('make:html-prettify', () => {
  return gulp.src(config.paths.build + '**/*.html')
    .pipe(prettify({
      indent_size: 2,
      max_preserve_newlines: 1
    }))
    .pipe(gulp.dest(config.paths.build));
});
