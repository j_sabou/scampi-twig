// Scampi Twig Gulpfile


// ----------------------------------------------------------------------------
// Pour connaître l'ensemble des tâches disponibles :
//
// $ gulp --tasks
//
// ----------------------------------------------------------------------------

const gulp         = require('gulp');

// Import des taches du repertoire tasks
const clean   = require('./tasks/clean.js');
const assets  = require('./tasks/assets.js');
const html    = require('./tasks/html.js');
const css     = require('./tasks/css.js');
const script  = require('./tasks/scripts.js');
const watch   = require('./tasks/watch.js');
const svg     = require('./tasks/svg.js');
const build   = require('./tasks/build.js');
