# Scampi-Twig

[Scampi-Twig](https://pidila.gitlab.io/scampi-twig) est un outil de création de pages statiques ou de gabarits d’intégration html/css/js utilisant la bibliothèque de composants [Scampi](https://gitlab.com/pidila/scampi) pour les CSS et Twig pour la génération des pages html. Il est développé et maintenu par le [PiDILA](https://pidila.gitlab.io), le Pôle intégration html de la Direction de l'information légale et administrative.

Scampi-Twig autorise la saisie des contenus par inclusion de fragments, en saisie twig/html directe ou par inclusion de fichiers markdown.

[Documentation complète](https://pidila.gitlab.io/scampi-twig)

Prérequis
------------------------------------------------------------------------------

Scampi-Twig a été testé et fonctionne avec :
Nodejs 13.11.x, npm 6.14.x et gulp 4.x.x.


Utilisation - Initialisation
------------------------------------------------------------------------------

### Créer un nouveau projet

Forker le dépôt ou télécharger l'archive et suivre la documentation : [commencer un nouveau projet](https://pidila.gitlab.io/scampi-twig/installation.html).


### Contribuer à Scampi-Twig

```bash
$ git clone https://gitlab.com/pidila/scampi-twig.git <mon_projet>
$ cd mon_projet && npm install && npm start
```

N'hésitez pas à créer une branche et proposer un pull request :)


Utiliser Scampi-Twig
------------------------------------------------------------------------------

### Récapitulatif des tâches gulp principales

On accède à la liste des tâches gulp disponibles et leur résumé à l'aide de la commande `gulp --tasks`.

  * Tout construire   :   `gulp build`
  * Tout nettoyer     :   `gulp clean`
  * Compiler sass     :   `gulp make:css`
  * Compiler twig     :   `gulp make:html`
  * Concaténer les js :   `gulp make:js-main`

Pendant les développements on peut lancer une tâche qui builde puis lance un serveur, ouvre la page d’index et surveille les modifications effectuées sur les css et le html :

  * `gulp dev`

### Création des pages html

Le fichier `dev/templates/common/layout-base.twig` rassemble les éléments communs à tous les projets.

Le fichier `dev/templates/common/layout-base-projet.twig` hérite de la base.twig et ajoute les particularités communes à toutes les pages d’un projet.

Le fichier `dev/pages/index.twig` sert de centre d’aiguillage pour les pages prêtes à être recettées.

Le répertoire *dev/templates/pages/* accueille les pages complètes. Trois pages sont livrées avec le dépôt : celle pour le styleguide, qui sera à compléter avec les éléments propres au projet, la page d'index et une page 404.

* tâche unitaire : `gulp make:html`


#### Prise en charge de markdown

Les contenus en markdown peuvent être insérés de deux façons :

  * en saisie directe dans un template :
  
  ```
  {% markdown %}
    Contenu
  {% endmarkdown %}
  ```

  * par inclusion de fichier depuis un template :
  
  ```
  {% markdown "chemin/relatif/depuis/templates" %}{% endmarkdown %}
  ```


### Gestion des scripts

Une tâche gulp prend en charge les scripts issus des modules de scampi et les scripts additionnels du projet pour les concaténer et les déplacer dans assets/scripts :

* pour les scripts issus des modules de scampi, selon la liste définie dans `tasks/scripts.js` ;
* pour les scripts additionnels : les fichiers placés dans `dev/assets/project/scripts/main`
  par défaut ce répertoire contient les scripts [anchor-focus](https://pidila.gitlab.io/scampi/documentation/anchor-focus.html) et has-js (détection de javascript).

Le fichier concaténé est envoyé dans `public/assets/scripts/main.js`.


### Compilation des css

Un fichier prêt à l’emploi est présent dans *dev/_assets/projet/scss*. Pour personnaliser un projet :

  1. dans *style.css* décommenter les imports de modules scampi utilisés tels quels ;
  2. ajouter les modules et partials propres au projet.

Tâches gulp :

  * tâche unitaire : `gulp make:css`


### Création du sprite rassemblant les icônes svg
------------------------------------------------------------------------------

Si les gabarits comportent des icônes au format svg, elles doivent être placées dans *dev/_assets/project/icones/unitaires/*. À titre d’exemple, 4 icônes y sont déjà placées.

Une tâche gulp permet de générer depuis ce répertoire un sprite d’icones svg à utiliser dans les gabarits :

  * `gulp prep:sprite` transforme les icones unitaires en sprite *(icon-sprite.svg)* et le place dans *dev/_assets/project/icones/*

Le sprite sera déplacé avec les autres ressources statiques quand on effectuera la commande gulp générale `gulp copy:assets`.

Voir aussi le [tutoriel pour la préparation et la fabrication du sprite d'icônes](https://pidila.gitlab.io/scampi-twig/documentation/sprite-svg.html).


### Déplacer les fichiers statiques

Les éléments du répertoire *dev/_assets/project* (à l’exclusion des scss et des scripts) sont déplacés tels quels dans le répertoire public grâce à des tâches gulp.

  * `gulp copy:assets` pour tous les éléments du répertoire _assets/project
  * `gulp copy:favicon` place la favicon à la racine du site




Déploiement sur <user>.gitlab.io/<projet>
------------------------------------------------------------------------------

Placer à la racine du dépôt un fichier *.gitlab-ci.yml* avec le contenu suivant :

```yaml
image: node
pages:
  stage: deploy
  script:
  - npm install
  - ./node_modules/.bin/gulp build:public
  artifacts:
    paths:
    - public
  only:
  - master
```

  * image : équipe le package docker qui sera utilisé pour "mouliner" les fichiers avant de les envoyer sur le serveur
  * pages : crée une tâche nommée *pages* qui demande au "runner" d’effectuer sur les fichiers du dépôt les tâches définies par la section *script* puis de déployer des fichiers produits dans un répertoire *public* dès qu’un commit est poussé sur *master*.

On peut surveiller ces opérations dans l’onglet CI / CD > jobs du dépôt.

Pour plus d’informations, voir [ce tuto pas à pas](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/).

**À noter :** Les pages déployées ne peuvent être protégées par un mot de passe, même si le dépôt est privé. Penser à ajouter des metas attributs noindex nofollow dans le head des pages.


Contact
------------------------------------------------------------------------------

Nous écrire : pidila@dila.gouv.fr

### Liste d’échanges et d’information :

[S’abonner](https://framalistes.org/sympa/subscribe/pidila-tools).


Licence
------------------------------------------------------------------------------

Scampi-twig est distribué sous une double licence :[MIT](https://gitlab.com/pidila/scampi-twig/blob/master/LICENCE-MIT.md) et [CeCILL-B](http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html).

Vous pouvez utiliser Scampi avec l’une ou l’autre licence.
